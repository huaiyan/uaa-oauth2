package org.huaiyan.oauth.uaa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 @SpringBootApplication 标识这是一个boot启动类
 * @date 2021/10/18 15:41
 * @author huaiyan
 * @return
 */
@SpringBootApplication
@MapperScan("org.huaiyan.oauth.uaa")
public class Oauth2Application {

    public static void main(String[] args) {
        SpringApplication.run(Oauth2Application.class, args);
    }

}
