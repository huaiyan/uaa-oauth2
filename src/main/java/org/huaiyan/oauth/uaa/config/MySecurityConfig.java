package org.huaiyan.oauth.uaa.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.authentication.AuthenticationManagerFactoryBean;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * Created with IntelliJ IDEA 2020.3
 *
 * @EnableWebSecurity 启用webSecurity 服务
 * @author: huaiyan
 * Date: 2021-10-18 17:42
 * Description: 文件描述信息
 */
@Configuration
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

    public static final Logger logger = LoggerFactory.getLogger(MySecurityConfig.class);

    /**
     * 配置安全资源防御
     *
     * @param http 安全请求
     * @return void
     * @date 2021/10/19 14:40
     * @author huaiyan
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                /**
                 * 放行一些端口
                 */
                .antMatchers("/oauth/**", "/login", "logout")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll()
                .and()
                .cors().disable();
    }

    /**
     * 开启密码模式
     *
     * @param
     * @return org.springframework.security.authentication.AuthenticationManager
     * @date 2021/10/19 14:36
     * @author huaiyan
     */
    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
}
