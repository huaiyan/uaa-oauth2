package org.huaiyan.oauth.uaa.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;


/**
 * Created with IntelliJ IDEA 2020.3
 *
 * @EnableResourceServer 表示当前服务是资源服务
 * @author: huaiyan
 * Date: 2021-10-19 11:12
 * Description: 文件描述信息
 */
@Configuration
@EnableResourceServer
public class MyResourceServiceConfig extends ResourceServerConfigurerAdapter {

    public static final Logger logger = LoggerFactory.getLogger(MyResourceServiceConfig.class);

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.cors().disable()
                .authorizeRequests()
                /**
                 * 任何资源需要认证,让请求去找授权服务器
                 */
                .anyRequest().authenticated()
                .and()
                /**
                 * 资源服务器的放行地址
                 */
                .requestMatchers()
                .antMatchers("/user/**");
    }
}
