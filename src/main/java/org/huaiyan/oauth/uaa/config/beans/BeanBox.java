package org.huaiyan.oauth.uaa.config.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.stereotype.Component;

import java.io.Serializable;


/**
 * Created with IntelliJ IDEA 2020.3
 *
 * @author: huaiyan
 * Date: 2021-10-18 18:03
 * Description: 文件描述信息
 */
@Configuration
public class BeanBox implements Serializable {

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    @Bean("redisTokenStore")
    public TokenStore redisTokenStore() {
        return new RedisTokenStore(redisConnectionFactory);
    }

    @Bean("jwtTokenStore")
    @Primary
    public TokenStore jwtTokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean("jwtAccessTokenConverter")
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
//        JwtAccessTokenConverter tokenConverter = new UserJwtAccessTokenConverter();
//        tokenConverter.setKeyPair(keyStoreKeyFactory.getKeyPair("eimc-jwt"));
//        读取私钥,放进令牌中
//        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("eimc-jwt.jks"), "passw0rd".toCharArray());

        JwtAccessTokenConverter tokenConverter = new JwtAccessTokenConverter();
        ///需要设置jwt的秘钥
        tokenConverter.setSigningKey("HUAI-YAN");
        return tokenConverter;
    }

    @Bean
    public JwtTokenEnhancer jwtTokenEnhancer() {
        return new JwtTokenEnhancer();
    }


}
