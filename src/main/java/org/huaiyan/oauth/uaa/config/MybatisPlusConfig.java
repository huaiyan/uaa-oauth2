package org.huaiyan.oauth.uaa.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;


/**
 * Created with IntelliJ IDEA 2020.3
 *
 * @author: huaiyan
 * Date: 2021-10-18 17:43
 * Description: 文件描述信息
 */
@Configuration
public class MybatisPlusConfig {

    public static final Logger logger = LoggerFactory.getLogger(MybatisPlusConfig.class);

}
