package org.huaiyan.oauth.uaa.services;

import org.huaiyan.oauth.uaa.dao.pojo.TabMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
public interface TabMenuService extends IService<TabMenu> {

}
