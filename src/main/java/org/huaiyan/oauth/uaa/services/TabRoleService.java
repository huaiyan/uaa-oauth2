package org.huaiyan.oauth.uaa.services;

import org.huaiyan.oauth.uaa.dao.pojo.TabRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
public interface TabRoleService extends IService<TabRole> {

}
