package org.huaiyan.oauth.uaa.services;

import org.huaiyan.oauth.uaa.dao.pojo.TabAdminUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户表 服务类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-18
 */
public interface TabAdminUserService extends IService<TabAdminUser> {

}
