package org.huaiyan.oauth.uaa.services;

import org.huaiyan.oauth.uaa.dao.pojo.TabRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
public interface TabRoleMenuService extends IService<TabRoleMenu> {

}
