package org.huaiyan.oauth.uaa.services.impl;

import org.huaiyan.oauth.uaa.dao.pojo.TabMenu;
import org.huaiyan.oauth.uaa.dao.mapper.TabMenuMapper;
import org.huaiyan.oauth.uaa.services.TabMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
@Service
public class TabMenuServiceImpl extends ServiceImpl<TabMenuMapper, TabMenu> implements TabMenuService {

}
