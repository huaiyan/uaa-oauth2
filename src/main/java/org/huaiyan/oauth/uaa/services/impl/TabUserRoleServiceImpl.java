package org.huaiyan.oauth.uaa.services.impl;

import org.huaiyan.oauth.uaa.dao.pojo.TabUserRole;
import org.huaiyan.oauth.uaa.dao.mapper.TabUserRoleMapper;
import org.huaiyan.oauth.uaa.services.TabUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
@Service
public class TabUserRoleServiceImpl extends ServiceImpl<TabUserRoleMapper, TabUserRole> implements TabUserRoleService {

}
