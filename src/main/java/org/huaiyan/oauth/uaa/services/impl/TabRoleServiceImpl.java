package org.huaiyan.oauth.uaa.services.impl;

import org.huaiyan.oauth.uaa.dao.pojo.TabRole;
import org.huaiyan.oauth.uaa.dao.mapper.TabRoleMapper;
import org.huaiyan.oauth.uaa.services.TabRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
@Service
public class TabRoleServiceImpl extends ServiceImpl<TabRoleMapper, TabRole> implements TabRoleService {

}
