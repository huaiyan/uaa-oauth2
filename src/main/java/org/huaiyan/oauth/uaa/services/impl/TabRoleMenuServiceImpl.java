package org.huaiyan.oauth.uaa.services.impl;

import org.huaiyan.oauth.uaa.dao.pojo.TabRoleMenu;
import org.huaiyan.oauth.uaa.dao.mapper.TabRoleMenuMapper;
import org.huaiyan.oauth.uaa.services.TabRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
@Service
public class TabRoleMenuServiceImpl extends ServiceImpl<TabRoleMenuMapper, TabRoleMenu> implements TabRoleMenuService {

}
