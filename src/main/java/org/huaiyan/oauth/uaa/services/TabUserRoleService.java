package org.huaiyan.oauth.uaa.services;

import org.huaiyan.oauth.uaa.dao.pojo.TabUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
public interface TabUserRoleService extends IService<TabUserRole> {

}
