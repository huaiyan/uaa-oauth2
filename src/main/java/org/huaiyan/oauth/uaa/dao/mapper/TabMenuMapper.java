package org.huaiyan.oauth.uaa.dao.mapper;

import org.huaiyan.oauth.uaa.dao.pojo.TabMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
public interface TabMenuMapper extends BaseMapper<TabMenu> {

}
