package org.huaiyan.oauth.uaa.dao.mapper;

import org.huaiyan.oauth.uaa.dao.pojo.TabRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
public interface TabRoleMapper extends BaseMapper<TabRole> {

}
