package org.huaiyan.oauth.uaa.dao.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.huaiyan.oauth.uaa.dao.pojo.TabAdminUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.huaiyan.oauth.uaa.dao.pojo.TabRole;
import org.huaiyan.oauth.uaa.dao.pojo.TabUser;

import java.util.List;

/**
 * <p>
 * 后台用户表 Mapper 接口
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-18
 */
public interface TabAdminUserMapper extends BaseMapper<TabAdminUser> {


    
    /**
     * 根据用户查询用户
     * @param username 用户名
     * @date 2021/10/19 14:17
     * @author huaiyan
     * @return org.huaiyan.oauth.uaa.dao.pojo.TabUser
     */
    @Select("SELECT * FROM `tab_admin_user` WHERE username =#{username} ")
    TabUser findUserByUsername(@Param("username") String username);

    /**
     * 根据用户查询具备那些角色
     *
     * @param id 用户ID
     * @return java.util.List<org.huaiyan.oauth.uaa.dao.pojo.TabRole>
     * @date 2021/10/19 10:55
     * @author huaiyan
     */
    @Select("SELECT  r.* FROM tab_role r , tab_user_role u WHERE 1=1 AND  r.role_id = u.role_id AND u.user_id =#{userId}")
    List<TabRole> selectRolesByUserId(@Param("userId") Integer id);



}
