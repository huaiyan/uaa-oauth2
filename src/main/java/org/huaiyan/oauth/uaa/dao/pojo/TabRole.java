package org.huaiyan.oauth.uaa.dao.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TabRole对象", description="角色表")
public class TabRole implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "角色主键ID")
    @TableId(value = "role_id", type = IdType.AUTO)
    private Integer roleId;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "角色描述")
    private String description;

    @ApiModelProperty(value = "角色中文名")
    private String nameZh;

    @ApiModelProperty(value = "角色展示排序")
    private Integer roleSort;

    @ApiModelProperty(value = "角色数据范围(1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限)")
    private String dataScope;

    @ApiModelProperty(value = "菜单树选择项是否关联显示")
    private Boolean menuCheckStrictly;

    @ApiModelProperty(value = "部门树选择项是否关联显示")
    private Boolean deptCheckStrictly;

    @ApiModelProperty(value = "角色是否启用(0:启用,1:不启用)")
    private Boolean isEnable;

    @ApiModelProperty(value = "角色是否逻辑删除(0:否,1:是)")
    @TableLogic
    private Boolean isDeleted;

    @ApiModelProperty(value = "角色备注信息")
    private String remark;

    @ApiModelProperty(value = "角色创建人")
    private String createBy;

    @ApiModelProperty(value = "角色更新人")
    private String updateBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

}
