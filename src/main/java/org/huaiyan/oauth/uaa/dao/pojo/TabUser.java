package org.huaiyan.oauth.uaa.dao.pojo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.*;

/**
 * <p>
 * 后台用户表
 * </p>
 *
 * @author huaiyan
 * @since 2021-10-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TabAdminUser对象", description="后台用户表")
public class TabUser implements Serializable, UserDetails {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户主键ID")
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    @ApiModelProperty(value = "部门ID(默认是-1表示没有部门)")
    private Integer deptId;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户密码")
    private String password;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户手机号")
    private String phoneNumber;

    @ApiModelProperty(value = "用户头像")
    private String headPic;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "微信OPENID")
    private String openId;

    @ApiModelProperty(value = "微信号")
    private String wechatName;

    @ApiModelProperty(value = "邮箱地址")
    private String email;

    @ApiModelProperty(value = "用户年龄(默认永远18岁)")
    private Integer age;

    @ApiModelProperty(value = "用户性别（0男 1女 2未知）")
    private String sex;

    @ApiModelProperty(value = "是否是超级管理员(0:否,1:是)")
    private Boolean isAdmin;

    @ApiModelProperty(value = "账号是否停用(0:否,1:是)")
    private Boolean isLocked;

    @ApiModelProperty(value = "是否删除(0:否,1:是)")
    @TableLogic
    private Boolean isDeleted;

    @ApiModelProperty(value = "是否启用(0:启用,1:禁用)")
    private Boolean isEnable;

    @ApiModelProperty(value = "账户是否过期(0:未过期,1:已过期)")
    private Boolean isExpired;

    @ApiModelProperty(value = "当前登录地址")
    private String loginIp;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新操作人")
    private String updateBy;

    @ApiModelProperty(value = "当前登录时间")
    private Date loginTime;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /**
     * 角色，一个用户可能有多个角色
     */
    List<TabRole> roles;
    /**
     * 表示账户是否未过期
     * 数据库有这个字段的话就直接返回这个字段
     * 处理机制（false：已过期，true:没有过期），默认是false
     *
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账户是否未锁定
     * 处理机制（false：已锁定，true:没有锁定），默认是false
     *
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return !isLocked;
    }
    /**
     * 凭证是否未过期
     * 处理机制（false：已过期，true:没有过期），默认是false
     *
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 账户是否启用
     *
     * @return
     */
    @Override
    public boolean isEnabled() {
        return  !isEnable;
    }
    /**
     * 返回用户的所有角色
     * 因为查询出来的是一个集合，不方便处理，我们数据库查询出来之后自己再整理下
     *
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        final boolean flag = null != roles && roles.size() > 0;
        if (flag) {
            // 注意角色名是否包含RILE_ ，不包含的话需要加上，不然会莫名地出错
            roles.stream().forEach(s -> authorities.add(new SimpleGrantedAuthority(Optional.ofNullable(s.getRoleName()).get())));
        }
        return authorities;
    }


}
