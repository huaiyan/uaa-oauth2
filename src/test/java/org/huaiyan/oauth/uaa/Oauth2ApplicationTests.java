package org.huaiyan.oauth.uaa;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class Oauth2ApplicationTests {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    void contextLoads() {
        System.out.println(passwordEncoder.encode("123456"));
    }

    @Test
    void generator(){
        AutoGenerator generator = new AutoGenerator();
        // 配置策略  GlobalConfig
        // 1：全局配置
        GlobalConfig config = new GlobalConfig();
        // 2：当前的项目路径
        String propertyPath = System.getProperty("user.dir");
        config.setOutputDir( propertyPath+ "/src/main/java");
        config.setAuthor("huaiyan");
        // 生成之后是否打开资源管理器 -- 打开文件夹
        config.setOpen(false);
        // 实体属性 swagger2注解
        config.setSwagger2(true);
        // 是否覆盖
        config.setFileOverride(true);
        // 去除Service 的I前缀
        config.setServiceName("%sService");
        // 设置日期的类型
        config.setDateType(DateType.ONLY_DATE);
        // 设置ID的类型
        config.setIdType(IdType.AUTO);
        // 1：将配置交给 AutoGenerator 管理
        generator.setGlobalConfig(config);
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setUrl("jdbc:mysql://localhost:13306/cjzb_cloud?useSSL=false");
        //我的mysql是5.7版本的
        dataSourceConfig.setDriverName("com.mysql.jdbc.Driver");
        //这是mysql8.0以上版本的
//        dataSourceConfig.setDriverName("com.mysql.cj.jdbc/Driver");
//        dataSourceConfig.setDriverName("com.alibaba.druid.pool.DruidDataSource");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("root123");
//        dataSourceConfig.setUrl("jdbc:mysql://182.254.209.120:3306/db_yh?useUnicode=true&characterEncoding=UTF-8&amp;useSSL=false&amp;jdbcCompliantTruncation=false&amp;zeroDateTimeBehavior=convertToNull&amp;allowMultiQueries=true");
//        dataSourceConfig.setUsername("root");
//        dataSourceConfig.setPassword("ThvFb7fNt");
        // 设置数据库类型
        dataSourceConfig.setDbType(DbType.MYSQL);
        // 2：将数据源配置交给 AutoGenerator 管理
        generator.setDataSource(dataSourceConfig);
        PackageConfig packageInfo = new PackageConfig();
        // 设置模块名称,也可以不设置
//        packageInfo.setModuleName("sale");
        // 包名，
        packageInfo.setParent("org.huaiyan.oauth.uaa");
        // 实体类
        packageInfo.setEntity("dao.pojo");
        packageInfo.setMapper("dao.mapper");
        packageInfo.setService("services");
        packageInfo.setServiceImpl("services.impl");
//        packageInfo.setController("controller");
        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        // 如果模板引擎是 freemarker
//        String templatePath = "/templates/mapper.xml.ftl";
//         如果模板引擎是 velocity
        String templatePath = "/templates/mapper.xml.vm";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return propertyPath + "/src/main/resources/mapper/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        generator.setCfg(cfg);
        packageInfo.setXml(null);
        // 3：设置包信息交给 AutoGenerator 管理
        generator.setPackageInfo(packageInfo);
        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setXml(null);
        generator.setTemplate(templateConfig);
        // 4：策略配置
        StrategyConfig strategy = new StrategyConfig();
        // 需要生成那些表,重点修改的地方
        // strategy.setInclude(); 表示生成全表
//        strategy.setInclude("tab_admin_user");
        // 登录信息表
        strategy.setInclude("tab_role");
        strategy.setInclude("tab_role_menu");
        strategy.setInclude("tab_user_role");
        strategy.setInclude("tab_menu");
        // 设置表前缀
//        strategy.setTablePrefix("tab_");
        // 表前缀转驼峰
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 列名转驼峰
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        //自动填充策略
        TableFill tableInsert = new TableFill("create_time", FieldFill.INSERT);
        TableFill tableUpdate = new TableFill("update_time", FieldFill.UPDATE);
        List<TableFill> list = new ArrayList<>();
        list.add(tableInsert);
        list.add(tableUpdate);
        // 5:将自动填充策略交给 AutoGenerator 管理
        strategy.setTableFillList(list);
        // 6：设置乐观锁
        strategy.setVersionFieldName("version");
        // 7:设置rest风格 默认是false
        strategy.setRestControllerStyle(false);
        // 比如： localhost:8089/hello_id_2,默认是false
        strategy.setControllerMappingHyphenStyle(false);
        // 自动生成lombok
        strategy.setEntityLombokModel(true);
        // 是否设置rest接口,默认是false
//        strategy.setRestControllerStyle(true);
        // 如果有逻辑删除字段，那么可以配置,配置之后那么就有自动填充策略
        strategy.setLogicDeleteFieldName("is_deleted");
        // 是否为构建者模型
        strategy.setChainModel(false);

        // 8:将所有的策略交给 AutoGenerator 管理
        generator.setStrategy(strategy);
        // 执行
        generator.execute();
    }


//    public static void main(String[] args) {
//
//        AutoGenerator generator = new AutoGenerator();
//        // 配置策略  GlobalConfig
//        // 1：全局配置
//        GlobalConfig config = new GlobalConfig();
//        // 2：当前的项目路径
//        String propertyPath = System.getProperty("user.dir");
//        config.setOutputDir( propertyPath+ "/src/main/java");
//        config.setAuthor("huaiyan");
//        // 生成之后是否打开资源管理器 -- 打开文件夹
//        config.setOpen(false);
//        // 实体属性 swagger2注解
//        config.setSwagger2(true);
//        // 是否覆盖
//        config.setFileOverride(true);
//        // 去除Service 的I前缀
//        config.setServiceName("%sService");
//        // 设置日期的类型
//        config.setDateType(DateType.ONLY_DATE);
//        // 设置ID的类型
//        config.setIdType(IdType.AUTO);
//        // 1：将配置交给 AutoGenerator 管理
//        generator.setGlobalConfig(config);
//        DataSourceConfig dataSourceConfig = new DataSourceConfig();
//        dataSourceConfig.setUrl("jdbc:mysql://localhost:13306/cjzb_cloud?useSSL=false");
//        //我的mysql是5.7版本的
//        dataSourceConfig.setDriverName("com.mysql.jdbc.Driver");
//        //这是mysql8.0以上版本的
////        dataSourceConfig.setDriverName("com.mysql.cj.jdbc/Driver");
////        dataSourceConfig.setDriverName("com.alibaba.druid.pool.DruidDataSource");
//        dataSourceConfig.setUsername("root");
//        dataSourceConfig.setPassword("root123");
////        dataSourceConfig.setUrl("jdbc:mysql://182.254.209.120:3306/db_yh?useUnicode=true&characterEncoding=UTF-8&amp;useSSL=false&amp;jdbcCompliantTruncation=false&amp;zeroDateTimeBehavior=convertToNull&amp;allowMultiQueries=true");
////        dataSourceConfig.setUsername("root");
////        dataSourceConfig.setPassword("ThvFb7fNt");
//        // 设置数据库类型
//        dataSourceConfig.setDbType(DbType.MYSQL);
//        // 2：将数据源配置交给 AutoGenerator 管理
//        generator.setDataSource(dataSourceConfig);
//        PackageConfig packageInfo = new PackageConfig();
//        // 设置模块名称,也可以不设置
////        packageInfo.setModuleName("sale");
//        // 包名，
//        packageInfo.setParent("org.huaiyan.oauth.uaa");
//        // 实体类
//        packageInfo.setEntity("dao.pojo");
//        packageInfo.setMapper("dao.mapper");
//        packageInfo.setService("services");
//        packageInfo.setServiceImpl("services.impl");
////        packageInfo.setController("controller");
//        // 自定义配置
//        InjectionConfig cfg = new InjectionConfig() {
//            @Override
//            public void initMap() {
//                // to do nothing
//            }
//        };
//        // 如果模板引擎是 freemarker
////        String templatePath = "/templates/mapper.xml.ftl";
////         如果模板引擎是 velocity
//        String templatePath = "/templates/mapper.xml.vm";
//        // 自定义输出配置
//        List<FileOutConfig> focList = new ArrayList<>();
//        // 自定义配置会被优先输出
//        focList.add(new FileOutConfig(templatePath) {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
//                return propertyPath + "/src/main/resources/mapper/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
//            }
//        });
//        cfg.setFileOutConfigList(focList);
//        generator.setCfg(cfg);
//        packageInfo.setXml(null);
//        // 3：设置包信息交给 AutoGenerator 管理
//        generator.setPackageInfo(packageInfo);
//        // 配置模板
//        TemplateConfig templateConfig = new TemplateConfig();
//        templateConfig.setXml(null);
//        generator.setTemplate(templateConfig);
//        // 4：策略配置
//        StrategyConfig strategy = new StrategyConfig();
//        // 需要生成那些表,重点修改的地方
//        // strategy.setInclude(); 表示生成全表
////        strategy.setInclude("tab_admin_user");
//        // 登录信息表
//        strategy.setInclude("tab_role");
//        strategy.setInclude("tab_role_menu");
//        strategy.setInclude("tab_user_role");
//        strategy.setInclude("tab_menu");
//        // 设置表前缀
////        strategy.setTablePrefix("tab_");
//        // 表前缀转驼峰
//        strategy.setNaming(NamingStrategy.underline_to_camel);
//        // 列名转驼峰
//        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
//        //自动填充策略
//        TableFill tableInsert = new TableFill("create_time", FieldFill.INSERT);
//        TableFill tableUpdate = new TableFill("update_time", FieldFill.UPDATE);
//        List<TableFill> list = new ArrayList<>();
//        list.add(tableInsert);
//        list.add(tableUpdate);
//        // 5:将自动填充策略交给 AutoGenerator 管理
//        strategy.setTableFillList(list);
//        // 6：设置乐观锁
//        strategy.setVersionFieldName("version");
//        // 7:设置rest风格 默认是false
//        strategy.setRestControllerStyle(false);
//        // 比如： localhost:8089/hello_id_2,默认是false
//        strategy.setControllerMappingHyphenStyle(false);
//        // 自动生成lombok
//        strategy.setEntityLombokModel(true);
//        // 是否设置rest接口,默认是false
////        strategy.setRestControllerStyle(true);
//        // 如果有逻辑删除字段，那么可以配置,配置之后那么就有自动填充策略
//        strategy.setLogicDeleteFieldName("is_deleted");
//        // 是否为构建者模型
//        strategy.setChainModel(false);
//
//        // 8:将所有的策略交给 AutoGenerator 管理
//        generator.setStrategy(strategy);
//        // 执行
//        generator.execute();
//    }

}
