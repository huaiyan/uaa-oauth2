# uaa-oauth2

#### 介绍
​	 一个简单的授权认证系统,主要技术框架是springBoot  + JWT+ SpringSecurity +Oauth2 等技术搭建的一个授权认证系统框架(资源服务和授权认证服务在同一个项目中,线上肯定不是这样设计的,资源和授权是分开的)



#### 软件架构
软件架构说明:

springBoot 2.3.11.RELEASE 

redis 4.0

jwt  0.9.0

springSecurity  2.1.3

springcloudoauth2 2.1.3

mybatisplus 3.4.1

mysql 5.7.2


#### 安装教程

1.  准备好一个mysql
2.  准备好一个redis
3.  之后直接启动即可

#### 使用说明

1. 授权码演示流程是: 

   在浏览器中输入:http://localhost:8002/oauth/authorize?client_id=demo-client&response_type=code&redirect_uri=http://www.baidu.com

   得到CODE

2. 用code在postman中进行配

3. 密码模式

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
